- slug: backend-engineer
  name: Backend Engineer
  role_path: /job-families/engineering/backend-engineer/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: B
      comment: As of April 2019 we are at 70% of our hiring plan
    employee_satisfaction:
      grade: A
      comment: There are little-to-no complaints after the October 2018 compensation change
    external_comparables:
      grade: A
      comment: We are within 15% of our external compensation data
    internal_comparables: 
      grade: A
      comment: This benchmark is at or above those of comparable roles
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019.
- slug: frontend-engineer
  name: Frontend Engineer
  role_path: /job-families/engineering/frontend-engineer/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: C
      comment: We are seeing a high rise in offers declined due to low compensation
    employee_satisfaction:
      grade: C
      comment: There is sensitivity around the Backend Engineer benchmark being raised in October 2018 because it is a close peer to this role.
    external_comparables:
      grade: A
      comment: The role is at the 50th percentile based on our market data.
    internal_comparables:
      grade: B
      comment: This benchmark is at or above those of comparable roles
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-08-08
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019.
- slug: support-engineer
  name: Support Engineer
  role_path: /job-families/engineering/support-engineer/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: A
      comment: We are attracting top candidates, and are routinely on or above our hiring plan.
    employee_satisfaction:
      grade: B
      comment: There may be sensitivity around the Backend Engineer benchmark being raised in October 2018 because it is a peer to this role.
    external_comparables:
      grade: A
      comment: The role is at the 75th percentile based on our market data.
    internal_comparables:
      grade: B
      comment: This benchmark is at or above those of comparable roles
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019.
- slug: support-agent
  name: Support Agent
  role_path: /job-families/engineering/dotcom-support/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified.
    recruiting:
      grade: A
      comment: We are attracting top candidates, and are routinely on or above our hiring plan.
    employee_satisfaction:
      grade: B
      comment: Current team is very efficient globally, creating automations to solve repetitive issues, and eager for career path forward.
    external_comparables:
      grade: B
      comment: The role is at the 50th percentile based on our market data.
    internal_comparables:
      grade: C
      comment: We have no internal comparables.
    bonus:
      grade: B
      comment: Utilizing discretionary but should consider annual at senior levels when reviewing global approach.
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
- slug: test-automation-engineer
  name: Test Automation Engineer
  role_path: /job-families/engineering/test-automation-engineer/
  grades:
    materials:
      grade: C
      comment: The description and career development materials need to be unified. The title should be updated to Software Engineer in Test to reflect the team's skill sets and modern industry standards.
    recruiting:
      grade: B
      comment: We are attracting candidates and routinely on our hiring plan, though we have lost some candidates due to compensation expectations.
    employee_satisfaction:
      grade: B
      comment: Discussions around the title refresh have been brought up by team members. Existing hires have also held this title in their previous job experience.
    external_comparables:
      grade: C
      comment: We are at the 50th percentile based on our compensation data. We are not attracting candidates who hold Software Engineer in Test positions. We have lost candidates in the hiring pipeline.
    internal_comparables:
      grade: B
      comment: There may be sensitivity that there are Backend Engineers in the same department (Quality) who are on a different benchmark.
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-18
    recommendations:
      - Revisit benchmark with modern Test Engineering titles such as Software Engineer in Test.
      - Change the title to Software Engineer in Test.
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019.
- slug: security-analyst
  name: Security Analyst
  role_path: /job-families/engineering/security-analyst/
  grades:
    materials:
      grade: B
      comment: The differentiation between levels of this role need better definition.
    recruiting:
      grade: A
      comment: We are attracting great candidates and routinely at or ahead of our plan.
    employee_satisfaction:
      grade: A
      comment: We have heard little-to-no-complaints from individuals in this role about compensation.
    external_comparables:
      grade: B
      comment: We are at the 50th percentile based on our compensation data. 
    internal_comparables:
      grade: C
      comment: This benchmark is lower, compared to other Security Department benchmarks.
    bonus:
      grade: D
      comment: We have not had any bonuses for this role
  recommendation:
    date: 2019-05-29
    recommendations:
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019
- slug: security-engineer
  name: Security Engineer
  role_path: /job-families/engineering/security-engineer/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: A
      comment: We are attracting great candidates and routinely at or ahead of our plan.
    employee_satisfaction:
      grade: A
      comment: We have heard little-to-no-complaints from individuals in this role about compensation.
    external_comparables:
      grade: A
      comment: We are at the 75th percentile based on our compensation data
    internal_comparables:
      grade: A
      comment: This role is well compensated compared to adjecent roles like SRE and Backend Engineer.
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019
- slug: ux-designer
  name: Product Designer
  role_path: /job-families/engineering/product-designer/
  grades:
    materials:
      grade: C
      comment: The description and career development materials need to be unified. The title may be changed to Product Designer.
    recruiting:
      grade: B
      comment: We are meeting our hiring plan with great effort, but we are struggling to attract candidates with the desired qualifications.
    employee_satisfaction:
      grade: A
      comment: We have heard little-to-no complaints from employees
    external_comparables:
      grade: A
      comment: We are at the 85th percentile based on our compensation data.
    internal_comparables:
      grade: A
      comment: This benchmark is at or above those of comparable roles
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will consider changing the title
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019
- slug: ux-researcher
  name: UX Researcher
  role_path: /job-families/engineering/ux-researcher/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: B
      comment: We are meeting our hiring plan with great effort, but we are struggling to attract candidates with the desired qualifications.
    employee_satisfaction:
      grade: B
      comment: Most people are happy, but we can be always be better.
    external_comparables:
      grade: A
      comment: We are at the 85th percentile based on our compensation data.
    internal_comparables:
      grade: A
      comment: This role is above adjacent positions like Frontend Engineer.
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019
- slug: site-reliability-engineer
  name: Site Reliability Engineer
  role_path: /job-families/engineering/site-reliability-engineer/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: A
      comment: We are attracting top candidates amd are on or ahead of our hiring plan.
    employee_satisfaction:
      grade: A
      comment: There have been litte-to-no complaints since the SRE role replaced the Production Engineer role in 2018
    external_comparables:
      grade: A
      comment: We are at or above the 75th percentile based on external data
    internal_comparables:
      grade: A
      comment: This role is well compensated compared to adjecent roles like Backend Engineer and Security Engineer.
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019
- slug: database-reliability-engineer
  name: Database Reliability Engineer
  role_path: /job-families/engineering/database-reliability-engineer/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: A
      comment: We are attracting top candidates amd are on or ahead of our hiring plan.
    employee_satisfaction:
      grade: A
      comment: There have been litte-to-no complaints since the DBRE role replaced the DBA role in late 2018
    external_comparables:
      grade: A
      comment: We are at or above the 75th percentile based on external data
    internal_comparables:
      grade: A
      comment: This role is well compensated compared to adjecent roles like Site Reliability Engineer and Security Engineer.
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019
