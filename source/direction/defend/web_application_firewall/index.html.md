---
layout: markdown_page
title: "Category Vision - Web Application Firewall"
---

- TOC
{:toc}

## Description
A web application firewall (or WAF) filters, monitors, and blocks HTTP traffic to and from a web application. A WAF is differentiated from a regular firewall in that a WAF is able to filter the content of specific web applications while regular firewalls serve as a safety gate between servers. By inspecting HTTP traffic, it can prevent attacks stemming from web application security flaws, such as SQL injection, cross-site scripting (XSS), file inclusion, and security misconfigurations.

### Goal

GitLab's goal with WAF is to provide visibility into your applications, clusters, and the traffic they receive. By being able to see what is being sent to your systems, GitLab wants to empower you to either block malicious traffic or to act on it somehow.

Additionally, we want to make it possible to identify and update parts of your app that are subject to malicious traffic. In this way, even if malicious traffic bypasses the WAF, you can ensure the underlying application itself is resilient against attacks like SQL injection or XSS.

### Roadmap
[Roadmap Board](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Adefend)

## What's Next & Why
[Web Application Firewall for Kubernetes Cluster MVC](https://gitlab.com/gitlab-org/gitlab-ce/issues/65192)

## Competitive Landscape
TODO

## Analyst Landscape
TODO

## Top Customer Success/Sales Issue(s)

There is no feature available for this category.

## Top Customer Issue(s)

The category is very new, so we still need to engage customers and get feedback about their interests and priorities in this area.

## Top Vision Item(s)
TODO


## Upcoming Releases
TODO
